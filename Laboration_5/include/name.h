//
// Created by Isak Kristola on 2019-02-19.
//

#ifndef DT019G_NAME_H
#define DT019G_NAME_H

#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include "constants.h"

class Name{
private:
    std::string firstName;
    std::string lastName;

public:
    // Constructors
    Name() = default;                                                     // Default constructor
    Name(const std::string &pFirstName, const std::string &pLastName);    // Initializing constructor

    // Setters
    void setFirstName(const std::string &inFirst) { firstName=inFirst; }
    void setLastName(const std::string &inLast) { lastName=inLast; }

    // Getters
    std::string getFirstName() const { return firstName ;}
    std::string getLastName() const { return lastName ;}

    // Overload operators
    bool operator==(const Name &name) const;
    bool operator<(const Name &name) const;
};

// Overloading << and >> for file handling
std::ostream &operator<<(std::ostream &os, const Name &name);
std::istream &operator>>(std::istream &os, Name &name);

#endif //DT019G_NAME_H
