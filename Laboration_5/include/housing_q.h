// housing_q.h
// Laboration 5, Housing queue
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Header file for HousingQ clas

#ifndef DT019G_USER_INTERFACE_H
#define DT019G_USER_INTERFACE_H

#include <iostream>
#include <string>
#include <limits>
#include "queue.h"
#include "person.h"

class HousingQ{
private:
    // Data
    QList queue;
    int personsInQueue = 0;
    std::string fileName;

    // Member functions
    void printPerson(const Person &pers);
    void printMenuOptions();
    void printQueue();
    void searchPerson();
    void removePerson();
    void saveToFile();
    void enqueue();
    void dequeue();

public:
    // Constructors
    HousingQ() = default;

    // Member functions
    void run();
};

#endif //DT019G_USER_INTERFACE_H
