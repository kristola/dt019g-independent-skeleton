//
// Created by Isak Kristola on 2019-02-19.
//

#ifndef DT019G_CONSTANTS_H
#define DT019G_CONSTANTS_H

#include <vector>
#include <string>

const char DELIM = '|';

const std::vector<std::string> MENU_OPTIONS{
    "1: Add person to queue",
    "2: Offer housing",
    "3: Print housing queue",
    "4: Print personal information",
    "5: Remove person from queue",
    "6: Save queue",
    "7: Exit"
};

#endif //DT019G_CONSTANTS_H
