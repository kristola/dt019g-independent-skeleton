// queue.h
// Laboration 5, Housing queue
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Header file for QIterator, QList and Node classes

#ifndef DT019G_QUEUE_LIST_H
#define DT019G_QUEUE_LIST_H

#include "person.h"

typedef Person Item;

class Node;

// -----------------------
// QList class declaration
// -----------------------
class QIterator{
private:
    Node* node;
public:
    QIterator() : node(nullptr) {}        // Default constructor
    explicit QIterator(Node* n) : node(n) {} // Initializing constructor

    Item &operator* () const;
    QIterator &operator++();    // prefix ++i
    QIterator operator++ (int); // postfix i++
    bool operator!=(const QIterator &qi) const;
};

// -----------------------
// QList class declaration
// -----------------------
class QList {
private:
    Node *first, *last;

public:
    // Constructors & destructors
    QList(): first(nullptr), last(nullptr) {};
    ~QList();

    void enqueue(Item item);
    bool dequeue(Item &item);
    bool del(Item item);
    bool isEmpty() const;

    QIterator begin() const { return QIterator(first); }
    QIterator end() const { return QIterator(nullptr); }
};

#endif //DT019G_QUEUE_LIST_H
