//
// Created by Isak Kristola on 2019-02-19.
//

#include "address.h"
using namespace std;

Address::Address(const string &aStreet, const string &aPostal, const string &aCity) {
    // Initializing constructor that takes three strings as parameters
    streetAddress = aStreet;
    postalNr = aPostal;
    city = aCity;
}

bool Address::operator==(const Address &address) const{
    // Overloads ==

    string data1 = streetAddress + city + postalNr;                             // Temp string with member data
    string data2 = address.streetAddress + address.city + address.postalNr;     // for this and other Address object.
    transform(data1.begin(), data1.end(), data1.begin(), ::tolower);            // Makes lowercase
    transform(data2.begin(), data2.end(), data2.begin(), ::tolower);
    return data1 == data2;
}

bool Address::operator<(const Address &address) const{
    // Overloads <

    string data1 = city + " " + streetAddress, data2 = address.city + " " + address.streetAddress;  // Conc. member data
    transform(data1.begin(), data1.end(), data1.begin(), ::tolower);                                // Makes lowercase
    transform(data2.begin(), data2.end(), data2.begin(), ::tolower);
    return data1 < data2;
}

void Address::setStreetAddress(const string &inStr){
    // Gets input and sets member variable for street address. Takes a string as argument.
    streetAddress = inStr;
}


void Address::setPostalNr(const string &inPost){
    // Gets input and sets member variable for postal nr. Takes a string as argument.
    postalNr = inPost;
}


void Address::setCity(const string &inCity){
    // Gets input and sets member variable for city. Takes a string as argument.
    city = inCity;
}

ostream &operator<<(ostream &os, const Address &adr){
    // Gets member data and sends to output stream.

    os << adr.getStreetAddress() << DELIM;
    os << adr.getPostalNr() << DELIM;
    os << adr.getCity() << DELIM;

    return os;
}

istream &operator>>(istream &is, Address &adr){
    // Read from inputstream and assign member variables

    string tmpString;                       // Acts as a temporary container for data coming from input stream
    getline(is, tmpString, DELIM);
    adr.setStreetAddress(tmpString);
    getline(is, tmpString, DELIM);
    adr.setPostalNr(tmpString);
    getline(is, tmpString, DELIM);
    adr.setCity(tmpString);

    return is;
}