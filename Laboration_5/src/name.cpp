//
// Created by Isak Kristola on 2019-02-19.
//

#include "name.h"
using namespace std;

Name::Name(const string &fName, const string &lName) {
    // Initializing contructor that takes two strings as parameters
    firstName = fName;
    lastName = lName;
}

bool Name::operator==(const Name &name) const{
    // Overloads == operator

    string tmpName1 = lastName + " " + firstName, tmpName2 = name.lastName + " " + name.firstName;
    transform(tmpName1.begin(), tmpName1.end(), tmpName1.begin(), ::tolower);
    transform(tmpName2.begin(), tmpName2.end(), tmpName2.begin(), ::tolower);
    return (tmpName1 == tmpName2);
}

bool Name::operator<(const Name &name) const {
    //Overloads < operator

    string tmpName1 = lastName + " " + firstName, tmpName2 = name.lastName + " " + name.firstName;
    transform(tmpName1.begin(), tmpName1.end(), tmpName1.begin(), ::tolower);
    transform(tmpName2.begin(), tmpName2.end(), tmpName2.begin(), ::tolower);
    return tmpName1 < tmpName2;

}

ostream &operator<<(ostream &os, const Name &name){
    // Gets member data and sends to output stream

    os << name.getFirstName() << DELIM;
    os << name.getLastName() << DELIM;

    return os;
}

istream &operator>>(istream &is, Name &name){
    // Reads data from input stream and adds to name object

    string tmpString;
    getline(is, tmpString, DELIM);
    name.setFirstName(tmpString);
    getline(is, tmpString, DELIM);
    name.setLastName(tmpString);

    return is;
}