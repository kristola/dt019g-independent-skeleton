// housing_q.cpp
// Laboration 5, Housing queue
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Source file for HousingQ clas
#include "housing_q.h"

using namespace std;

void HousingQ::run(){

    cout << "Enter filename: ";
    cin >> fileName;

    // Reads information to queue if file exists
    Person tmpPerson;
    std::ifstream inf(fileName, std::ios::in);
    while (inf >> tmpPerson){
        queue.enqueue(tmpPerson);
        personsInQueue++;
    }
    inf.close();

    // Menu loop
    bool run = true;
    int userChoice;
    do {
        // MENU
        printMenuOptions();
        cin >> userChoice;
        cin.get();

        // Calls funcions based on user input
        switch(userChoice){
            case 1:
                enqueue();
                break;
            case 2:
                dequeue();
                break;
            case 3:
                printQueue();
                break;
            case 4:
                searchPerson();
                break;
            case 5:
                removePerson();
                break;
            case 6:
                saveToFile();
                break;
            case 7:
                run = false;
                break;
            default:
                cout << "Bad input, try again..\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        if (userChoice != 7) {
            cout << "\nPress ENTER to continue";
            cin.get();
        }
    } while (run);
}

void HousingQ::printMenuOptions(){

    cout << '\n' << "***** Welcome to the menu *****\n\n";
    for (auto &e : MENU_OPTIONS){                               // MENU_OPTIONS is defined in
        cout << e << '\n';                                      // constants.h
    }
    cout << "\nEnter your choice: ";
}

void HousingQ::printPerson(const Person &pers){

    // Gets data for subclasses
    Name tmpName = pers.getName();
    Address tmpAddress = pers.getAddress();

    // Print formatted member data
    cout << setw(15) << left << "Name: " << tmpName.getFirstName() << " " << tmpName.getLastName() << '\n';
    cout << setw(15) << left << "Address: " << tmpAddress.getStreetAddress() << '\n';
    cout << setw(15) << left << "Postcode:" << tmpAddress.getPostalNr() << " " << tmpAddress.getCity() << '\n';
    cout << setw(15) << left << "Person number: " << pers.getPersNr() << '\n';
    cout << setw(15) << left << "Shoe size: " << pers.getShoeNr() << '\n';
}

void HousingQ::enqueue(){

    cout << "***** Add person to queue *****\n\n";
    Person tmpPerson;

    // Get name
    string tmpFirst, tmpLast;
    cout << "First name: ";
    getline(cin, tmpFirst);
    cout << "Last name: ";
    getline(cin, tmpLast);
    tmpPerson.setName(tmpFirst, tmpLast);

    // Get address
    string tmpStreet, tmpPost, tmpCity;
    cout << "Address: ";
    getline(cin, tmpStreet);
    cout << "Zipcode: ";
    getline(cin, tmpPost);
    cout << "City: ";
    getline(cin, tmpCity);
    tmpPerson.setAddress(tmpStreet, tmpPost, tmpCity);

    // Get personal number and shoesize
    string tmpPn;
    int tmpShoe;
    cout << "Personal number: ";
    getline(cin, tmpPn);
    cout << "Shoe size: ";
    cin >> tmpShoe;
    tmpPerson.setPersNr(tmpPn);
    tmpPerson.setShoeNr(tmpShoe);
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');    // Flush buffer, remove newline left in stream

    queue.enqueue(tmpPerson);
    personsInQueue++;
}

void HousingQ::dequeue(){
    // Print and remove data for person first in queue
    cout << "\n***** Offer housing *****\n\n";

    // Exits if queue is empty
    if (queue.isEmpty()){
        cout << "Queue is empty.\n";
        return;
    }

    // Dequeues and assign data to tmpPerson
    Person tmpPerson;
    queue.dequeue(tmpPerson);
    personsInQueue--;

    // Print
    cout << "Person first in line: \n";
    printPerson(tmpPerson);
}

void HousingQ::printQueue(){

    cout << "\n***** Print queue *****\n\n";

    // Exits if queue is empty
    if (queue.isEmpty()){
        cout << "Queue is empty.\n";
        return;
    }

    // Iterate through queue and print every entry
    QIterator tmpIter = queue.begin();
    Person tmpPerson;
    int counter = 1;
    while (tmpIter != queue.end()){
        tmpPerson = *tmpIter++;
        printPerson(tmpPerson);
        cout << "Place in queue: " << counter++ << "\n\n";
    }
}
void HousingQ::searchPerson(){

    // Exits if queue is empty
    if (queue.isEmpty()){
        cout << "Queue is empty.\n";
        return;
    }

    cout << "\n***** Print person data *****\n\n";
    string tmpPersonNumber;
    cout << "Enter social security number for a person in queue: ";
    getline(cin, tmpPersonNumber);

    // Iterates through queue and look for person with matching social security number
    QIterator tmpIter = queue.begin();
    Person tmpPerson;
    int counter = 1;
    while (tmpIter != queue.end()){
        tmpPerson = *tmpIter++;
        if (tmpPersonNumber == tmpPerson.getPersNr()){                      // If a match is made
            cout << "Person found. Place in queue: " << counter << '\n';    // print person data
            printPerson(tmpPerson);
            return;
        }
        counter++;
    }
    cout << "\nNo person with mathing social security number found in queue.\n";
}

void HousingQ::removePerson(){

    // Exits if queue is empty
    if (queue.isEmpty()){
        cout << "Queue is empty.\n";
        return;
    }

    // Get user input
    cout << "***** Remove person from queue *****\n\n";
    string tmpPersonNumber;
    cout << "Enter social security number for a person in queue: ";
    getline(cin, tmpPersonNumber);

    // Iterates through queue and look for person with matching social security number
    QIterator tmpIter = queue.begin();
    Person tmpPerson;
    while (tmpIter != queue.end()){
        tmpPerson = *tmpIter++;
        if (tmpPersonNumber == tmpPerson.getPersNr()){                      // If a match is made
            cout << "Person found. Removing..\n";
            queue.del(tmpPerson);                                           // call delete function
            personsInQueue--;
            return;
        }
    }
    cout << "\nNo person with mathing social security number found in queue.\n";
}
void HousingQ::saveToFile(){

    cout << "Saving to file..";

    // Iterate through queue and print data
    // Creates temporary persons which are sent to ostream object
    QIterator tmpIter = queue.begin();
    Person tmpPerson;
    std::ofstream of(fileName, std::ios::out);
    while (tmpIter != queue.end()){
        tmpPerson = *tmpIter++;
        of << tmpPerson << '\n';
    }
    of.close();
}
