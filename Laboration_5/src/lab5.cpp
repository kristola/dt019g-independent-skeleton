// lab5.cpp
// Laboration 5, Housing Queue
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Main file

#include <iostream>
#include <string>
#include "housing_q.h"
#include "queue.h"
#include "person.h"

int main()
{
    HousingQ queue;
    queue.run();

    return 0;
}
