// queue.cpp
// Laboration 5, Housing queue
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Source file for QIterator, Qlist and Node classes

#include "queue.h"

class Node
{
public:
    Node *next;
    Item data;
    Node (Node *n, Item newData) : next(n), data(newData) {}
};

// ----------------------------------------
// Function definitions for class QIterator
// ----------------------------------------

// Overload dereference operator
Item &QIterator::operator* () const{
    return node->data;
}

// Overload prefix ++ operator
QIterator &QIterator::operator++(){
    node = node->next;
    return *this;
}

// Overload suffix ++ operator
QIterator QIterator::operator++(int){
    QIterator tmp = *this;
    node = node->next;
    return tmp;
}

// Overload inequality operator
bool QIterator::operator!=(const QIterator &qi) const{
    return node != qi.node;
}

// ------------------------------------
// Function definitions for class queue
// ------------------------------------

// Destructor
QList::~QList()
{
    while(!isEmpty())
    {
        Node *tmp = first;
        first = first->next;
        delete tmp;
    }
}

// Add data last in queue
void QList::enqueue(Item pData)
{
    Node *pNew = new Node(nullptr, pData);  // New Node points to NULL because it is last in queue
    if(isEmpty())
        first = pNew;
    else
        last->next = pNew;
    last = pNew;
}

bool QList::dequeue(Item &pData)
{
    if(isEmpty())
        return false;

    Node *n = first;        // Puts data from first node
    pData = n->data;        // in referenced pData
    first = first->next;
    if(isEmpty())           // If queue is now empty
        last = nullptr;
    delete n;
    return true;
}

// Check if queue is empty
bool QList::isEmpty() const
{
    return first == nullptr;
}

// Search and remove
bool QList::del(Item item){

    Node *prev = nullptr;
    for (Node *p=first; p; p=p->next){      // Iterate through array
        if (p->data == item){               // If node data matches search parameter
            if (first == last){             // If queue size = 1
                first = nullptr;
                last = nullptr;
            }
            else if (p == first){           // If item is first in queue
                first = p->next;
            }
            else if (p == last){            // If item is last in queue
                prev->next = nullptr;
                last = prev;
            }
            else {                          // If item is neither first or last in queue
                prev->next = p->next;
            }
            delete p;
        }
        prev = p;
    }
    return false;
}
