// person.cpp
// Laboration 5, Housing queue
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Source file for Person class

#include "person.h"
using namespace std;

Person::Person(const Name &aName, const Address &anAddress, const string &aPersNr, const int &aShoeNr){
    name = aName;
    address = anAddress;
    persNr = aPersNr;
    shoeNr = aShoeNr;
}

void Person::setName(std::string &newFirst, std::string &newLast){

    name.setFirstName(newFirst);
    name.setLastName(newLast);
}

void Person::setAddress(std::string &newAddr, std::string &newPost, std::string &newCity){

    address.setStreetAddress(newAddr);
    address.setPostalNr(newPost);
    address.setCity(newCity);
}

bool Person::operator<(const Person &person) const{
    // Overloads < operator

    if (name == person.name){
        return address < person.address;
    }
    return name < person.name;
}

bool Person::operator==(const Person &person) const{
    // Overloads == operator

    return name == person.name && address == person.address && persNr == person.persNr && shoeNr == person.shoeNr;
}

// ---------------
// Overloading
// ---------------

ostream &operator<<(ostream &os, Person &person){
    // Overloads << operator

    os << person.getName();
    os << person.getAddress();
    os << person.getPersNr() << DELIM;
    os << person.getShoeNr();

    return os;
}

istream &operator>>(istream &is, Person &person){
    // Overloads >> operator

    // Calls Name and Address member functions which handles reading from input stream
    person.inNames(is);
    person.inAddresses(is);

    string tmpString;                   // Temporary container for incoming data
    int tmpInt = 0;                     //              -||-
    getline(is, tmpString, DELIM);
    person.setPersNr(tmpString);
    is >> tmpInt;
    person.setShoeNr(tmpInt);
    is.get();

    return is;
}