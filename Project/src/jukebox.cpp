/*
 * jukebox.cpp
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Source file for class Jukebox
 */

#include "../include/jukebox.h"
using namespace std;
using namespace std::placeholders;  // For bind
using std::this_thread::sleep_for;
using std::chrono::milliseconds;


/*
 * Default constructor, initializes menus
 */
Jukebox::Jukebox(){

    // Initialize main menu
    string mainMenuHeader { "*** MAIN ***"};
    mainMenu.setMenuHeader(mainMenuHeader);
    mainMenu.addItem("1: Quit", true);
    mainMenu.addItem("2: File handling", true);
    mainMenu.addItem("3: Add an album", false);
    mainMenu.addItem("4: Delete an album", false);
    mainMenu.addItem("5: Print menu", false);
    mainMenu.addItem("6: Play menu", false);

    // Initialize file menu
    string fileMenuHeader {"*** FILE ***"};
    fileMenu.setMenuHeader(fileMenuHeader);
    fileMenu.addItem("1: Exit to main menu", true);
    fileMenu.addItem("2: Open file", true);
    fileMenu.addItem("3: Save to file", false);

    // Initialize print menu
    string printMenuHeader {"*** PRINT ***"};
    printMenu.setMenuHeader(printMenuHeader);
    printMenu.addItem("1: Exit to main menu", false);
    printMenu.addItem("2: Print a album", false);
    printMenu.addItem("3: Print albums and songs (in alphabetic order by album name)", false);
    printMenu.addItem("4: Print albums and songs (sorted by total track time)", false);
    printMenu.addItem("5: Print album names only (in alphabetic order by album name)", false);
    printMenu.addItem("6: Print album names only (sorted by total track time)", false);

    // Initialize play menu
    string playMenuHeader { "*** PLAY ***"};
    playMenu.setMenuHeader(playMenuHeader);
    playMenu.addItem("1: Exit to main menu", false);
    playMenu.addItem("2: Create playlist", false);
    playMenu.addItem("3: Generate random playlist", false);
    playMenu.addItem("4: Start playlist", false);

}


/*
 * Main loop
 */
void Jukebox::run(){

    bool again = true;
    do {
        mainMenu.printMenuItems();
        switch(mainMenu.getMenuChoice()){
            case 1:
                again = false;
                break;
            case 2:
                file();
                break;
            case 3:
                addAlbum();
                break;
            case 4:
                removeAlbum();
                break;
            case 5:
                print();
                break;
            case 6:
                play();
                break;
            default:
                break;
        }
    } while(again);
}


// ------------------------------------------------------------
// Main menu functions
// ------------------------------------------------------------

/*
 * Sub-menu File
 */
void Jukebox::file(){

    bool again = true;
    do {
        fileMenu.printMenuItems();
        switch(fileMenu.getMenuChoice()){
            case 1:
                again = false;
                break;
            case 2:
                cout << "Reading from file " << FILE_NAME << '\n';  // FILE_NAME is declared in constants.h
                OpenFromFile();
                break;
            case 3:
                cout << "Saving to file " << FILE_NAME << '\n';
                saveToFile();
                break;
            default:
                break;
        }
    } while (again);
}

/*
 * Sub-menu Print
 */
void Jukebox::print(){

    bool again = true;
    do {
        printMenu.printMenuItems();
        switch(printMenu.getMenuChoice()){
            case 1:
                again = false;
                break;
            case 2:
                printOneAlbum();
                break;
            case 3:
                printAll(sortOption{byName}, printOption{fullAlbum});
                break;
            case 4:
                printAll(sortOption{byLength}, printOption{fullAlbum});
                break;
            case 5:
                printAll(sortOption{byName}, printOption{titleOnly});
                break;
            case 6:
                printAll(sortOption{byLength}, printOption{titleOnly});
                break;
            default:
                break;
        }
    } while (again);
}

/*
 * Sub-menu Play
 */
void Jukebox::play(){

    bool again = true;
    do {
        playMenu.printMenuItems();
        switch(playMenu.getMenuChoice()){
            case 1:
                again = false;
                break;
            case 2:
                cout << "*** CREATE PLAYLIST ***\n";
                createPlaylist(byUserInput);
                break;
            case 3:
                cout << "*** GENERATE RANDOM PLAYLIST ***\n";
                createPlaylist(randomize);
                break;
            case 4:
                cout << "*** PLAYING PLAYLIST ***\n";
                playPlaylist();
                break;
            default:
                break;
        }
    } while (again);
}

/*
 * Prompts user to press ENTER before continuing
 */
void Jukebox::pressEnterToContinue(){
    cout << "\nPress <ENTER> to return to menu..\n";
//    string dummy;
//    cin >> dummy;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}


// ------------------------------------------------------------
// Play menu functions
// ------------------------------------------------------------

/*
 * Creates playlist from all songs
 * Input: const generateOption& (decides if playlist will be randomized or based on user choice)
 */
void Jukebox::createPlaylist(const generateOption &generateOption){

    // Create new vector of type Song and fill with all songs from all albums
    vector<Song> allSongs;
    for (auto &album : albumList){
        for (auto &song: album.getAlbumSongs()){
            allSongs.push_back(song);
        }
    }

    // Vector which will contain index of the songs that will be added to playlist
    vector<int> songIndexVector;

    if (generateOption == byUserInput) {
        printNumberedList(allSongs);
        songIndexVector = getUserSongChoices();
        }

    else if (generateOption == randomize){
        cout << "Number of songs in playlist: ";
        unsigned long numberOfSongsInPlaylist {0};
        cin >> numberOfSongsInPlaylist;

        if (numberOfSongsInPlaylist > allSongs.size()){
            cout << "Not enough songs in library!\n";
            return;
        }

        // Random number generator
        random_device rd;
        uniform_int_distribution<int> dist(1, static_cast<int>(allSongs.size()));

        // Generates random unique numbers in interval [1..allSongs.size()]
        // Adds numbers to songIndexVector
        while (songIndexVector.size() < numberOfSongsInPlaylist){
            int tmpInt {dist(rd)};
            auto it = find(songIndexVector.begin(), songIndexVector.end(), tmpInt); // Makes sure that only numbers
            if (it == songIndexVector.end()) {                                      // that already aren't in vector
                songIndexVector.push_back(tmpInt);                                  // will be added
            }
        }
    }

    // Create temporary Queue, add songs based on indexes, assign to playlist
    Queue tmpPlaylist;
    for (auto &songIndex : songIndexVector) {
        tmpPlaylist.enQueue(allSongs[songIndex - 1]);
    }
    playlist = tmpPlaylist;
}

/*
 * Prints songs in vector of songs in a numbered list
 * Input: const vector<Song>&
 */
void Jukebox::printNumberedList(const vector<Song> &songList){

    int counter {1};
    for_each(songList.begin(), songList.end(), [&counter] (Song s) {    // Takes a lambda function
        cout << counter++ << ": ";                                      // which handles printing
        s.printSong();
    });
}

/*
 * Gets user song choices
 * Output: vector<int> containing choices
 */
vector<int> Jukebox::getUserSongChoices(){

    cout << "\nEnter song numbers separated by \",\": ";
    string tmpString, delim {","};
    vector<int> parsedNumbers;
    getline(cin, tmpString);

    // Parse inputstring
    try {
        size_t position{0};
        while ((position = tmpString.find(',')) != string::npos) {
            parsedNumbers.push_back(stoi(tmpString.substr(0, position)));
            tmpString.erase(0, position + delim.length());
        }
        parsedNumbers.push_back(stoi(tmpString));
    } catch (const exception &e) {                                  // Catches exception thrown if input
        cout << "Bad input! Format must be [int,int,...,int]\n";    // format is wrong (and stoi fails)
        pressEnterToContinue();
    }

    return parsedNumbers;
}

/*
 * Plays playlist
 */
void Jukebox::playPlaylist(){

    if (playlist.isEmpty()) {
        cout << "Playlist is empty...\n";
        pressEnterToContinue();
        return;
    }

    Song tmpSong;
    Queue tmpPlaylist = playlist;                       // Plays from a playlist copy so original will be unchanged
    int delayInMilliseconds {3000};                     // songTime interval between "plays"
    while (!tmpPlaylist.isEmpty()) {
        cout << "Playing song: ";
        tmpSong = tmpPlaylist.deQueue();
        tmpSong.printSong();
        sleep_for(milliseconds(delayInMilliseconds));
    }
}


// ---------------------------------------------------------------------
// Search
// ---------------------------------------------------------------------

/*
 *  Search for album
 *  Input: Album name
 *  Output: Iterator pointing to album in albumList if found, else albumList.end()
 */
auto Jukebox::findAlbum(string targetAlbum){

    // Transform to lowercase for comparison purposes
    transform(targetAlbum.begin(), targetAlbum.end(), targetAlbum.begin(), ::tolower);

    // Search through albumList and use a lambda function to
    // to match albums from albumList against targetAlbum
    auto it = find_if(albumList.begin(), albumList.end(),
                      [targetAlbum] (const Album &album) {
                          string tmpAlbName = album.getAlbumName();
                          transform(tmpAlbName.begin(), tmpAlbName.end(), tmpAlbName.begin(), ::tolower);
                          return tmpAlbName == targetAlbum;
                      });
    return it;
}


// ----------------------------------------------------------
// Print
// ----------------------------------------------------------

/*
 * Prints one album based on user input
 */
void Jukebox::printOneAlbum(){

    // Get user input
    cout << "Enter album name: ";
    string albumName;
    getline(cin, albumName);

    // findAlbum returns a iterator pointing to album if found
    auto it = findAlbum(albumName);

    if (it != albumList.end()) {
        it->printAlbum(printOption{fullAlbum});
    }
    else{
        cout << "Album not found in list...\n";
        pressEnterToContinue();
    }
}

/*
 * Prints all albums in albumList. The type of print depends on input enums.
 * Input: const sortOption& (enum), const printOption& (enum)
 */
void Jukebox::printAll(const sortOption &sortOption, const printOption &printOption){

    sortAlbumList(sortOption);
    cout << "Album list: \n\n";

    // bind creates a function object which are used for printing
    for_each(albumList.begin(), albumList.end(), bind(&Album::printAlbum, _1, printOption));
}


// ---------------------------------------------------------------------
// Album handling
// ---------------------------------------------------------------------

/*
 * Add album to albumList
 */
void Jukebox::addAlbum(){

    cout << "*** ADD ALBUM ***\n";
    cout << "Enter album name: ";
    string albumName;
    getline(cin, albumName);

    // Search for possible duplicates in albumList
    auto it = findAlbum(albumName);
    if (it != albumList.end()){
        cout << "Album already exists in jukebox!\n";
        pressEnterToContinue();
        return;
    };

    cout << "Enter number of songs in album: ";
    int numberOfSongs {0};
    while (numberOfSongs <= 0) {        // Ensures input is a number > 0
        cin >> numberOfSongs;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    cout << "Add songs!\n";
    Album tmpAlbum(albumName);
    string songName, songArtist;
    int hour, minute, second;
    for (int i=0; i<numberOfSongs; i++){
        cout << "Song name: ";
        getline(cin, songName);
        cout << "Artist: ";
        getline(cin,songArtist);
        cout << "Length (h): ";
        cin >> hour;
        cout << "Length (m): ";
        cin >> minute;
        cout << "Length (s): ";
        cin >> second;

        // Checks that time values are positive numbers
        if (!(hour < 0 || minute < 0 || second < 0 || cin.fail())){
            tmpAlbum.addSong(Song(songName, songArtist, songTime(hour, minute, second)));
        }
        else{
            cout << "Bad input! songTime values must be numbers > 0. Try again.\n";
            i--;
        }
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    albumList.push_back(tmpAlbum);
}

/*
 * Removes album from albumList
 */
void Jukebox::removeAlbum(){

    // Get user input
    cout << "*** REMOVE ALBUM ***\n\n";
    cout << "Enter album name: ";
    string albumName;
    getline(cin, albumName);

    // Search for album in albumListk
    // findAlbum returns a iterator pointing to album if found
    auto it = findAlbum(albumName);

    if (it != albumList.end()) {                // if found
        albumList.erase(it);
        cout << "Album removed successfully\n";
    }
    else{
        cout << "Album not found in list..\n";
    }
}


// -----------------------------------------------------------------
// Sort
// -----------------------------------------------------------------

/*
 * Sorts albumList as specified by paramenter enum
 * Input: const sortOption& (enum)
 */
void Jukebox::sortAlbumList(const sortOption &sortOption){

    if (sortOption == byName){
        sort(albumList.begin(), albumList.end(),
             [](const Album &a, const Album &b){                                     // Lambda function used
                 string albumA {a.getAlbumName()}, albumB {b.getAlbumName()};        // for sorting albums.
                 transform(albumA.begin(), albumA.end(), albumA.begin(), ::tolower); // Compared objects are
                 transform(albumB.begin(), albumB.end(), albumB.begin(), ::tolower); // transformed to lowercase
                 return albumA < albumB;
             });
    }

    if (sortOption == byLength) {                       // Default comparison for sort is <
        sort(albumList.begin(), albumList.end());       // which is overloaded for album class.
        reverse(albumList.begin(), albumList.end());    // Changes from ascending to descending order
    }
}


// -----------------------------------------------------------------
// File Handling
// -----------------------------------------------------------------

/*
 * Read data from predetermined file (located in constants.h)
 * Input: none
 * Output: bool indicating whether read was successful or not
 */
bool Jukebox::OpenFromFile(){

    ifstream inFile(FILE_NAME);

    // Exits if file doesn't exist
    if (!inFile){
        cout << "Bad filename, exiting..\n";
        return false;
    }

    // Enable all menu options
    mainMenu.enableFullMenu();
    fileMenu.enableFullMenu();
    printMenu.enableFullMenu();
    playMenu.enableFullMenu();

    // Erase data in album list
    albumList.clear();

    // Read data to tmpAlbum and push to albumList
    Album tmpAlbum;
    while(inFile >> tmpAlbum){
        albumList.push_back(tmpAlbum);
        tmpAlbum.clear();
    }

    cout << "File opened successfully!\n";
    inFile.close();
    return true;
}

/*
 * Write data to predetermined file
 * Input: none
 * Output: bool indicating whether write was successful or not
 */
bool Jukebox::saveToFile(){

    cin.get();

    ofstream outFile(FILE_NAME);

    // All albums in list are written to output stream
    for (auto &album : albumList){
        outFile << album;
    }

    cout << "File saved successfully!\n";
    return true;
}


