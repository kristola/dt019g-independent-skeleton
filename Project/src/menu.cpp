//
// Created by Isak Kristola on 2019-02-25.
//

#include "../include/menu.h"
using namespace std;

/*
 * Add and item to menu
 * Input: const string& with menu options, const bool& deciding whether item should be enabled or not
 */
void Menu::addItem(const std::string &option, const bool &enabled){

    MenuItem tmp(option, enabled);
    menuItems.push_back(tmp);
}

/*
 * Prints menu items
 */
void Menu::printMenuItems(){

    cout << '\n' << header << '\n';
    for (auto &item:menuItems){
        if (item.isEnabled()){
            std::cout << item.getMenuItem() << '\n';
        }
    }
}

/*
 * Gets user input based on menu options
 * Output: int carrying user input
 */
int Menu::getMenuChoice(){

    cout << "\nEnter a number and press ENTER\n> ";
    int tmpInt = 0;
    while (tmpInt == 0) {
        cin >> tmpInt;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }


    // Checks that user option is enabled..
    if (menuItems[tmpInt-1].isEnabled()){
        return tmpInt;
    }

    // .. if its not, return -1
    cout << "Invalid option.\n";
    return -1;
}

/*
 * Enables all menu options
 */
void Menu::enableFullMenu(){

    for (auto &item : menuItems){
        item.enable();
    }
}
