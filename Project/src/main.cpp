/*
 * main.cpp
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Main file for Project
 */

#include <iostream>
#include "jukebox.h"

#include "songTime.h"


int main() {

    Jukebox jukebox;
    jukebox.run();

    return 0;
}