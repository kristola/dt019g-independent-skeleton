/*
 * queue.cpp
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Source file for class Queue
 */

#include "../include/queue.h"

using namespace std;

// -------------------------
// Constructors & destructor
// -------------------------


/*
 * Default constructor
 */
Queue::Queue(){

//    cout << "\nRunning default constructor\n";
    size = 5;
    ptr = new Song[size];          // Allocates space for array of [size] Song-objects
    first = last = -1;
}

/*
 * Copy constructor
 */
Queue::Queue(const Queue &otherQueue){

//    cout << "\nRunning copy constructor!\n";
    size = otherQueue.size;
    ptr = new Song[size];                                           // Allocate new memory
    first = otherQueue.first;
    last = otherQueue.last;
    copy(otherQueue.ptr, otherQueue.ptr + otherQueue.size, ptr);    // Copies all elements in a range
}

/*
 * Destructor
 */
Queue::~Queue(){

//    cout << "\nrunning queue destructor!\n";
    delete [] ptr;                                                  // Deallocate memory
}


// --------------------------------
// Overloaded operators
// --------------------------------

/*
 * Overload of = operator
 */
Queue& Queue::operator= (const Queue &otherQueue){

//    cout << "\nRunning assignment operator\n";
    if (this != &otherQueue) {                                          // Check for self-assignment
        Song* tempPtr = new Song[otherQueue.size];                      // Allocate new memory and copy elements
        first = otherQueue.first;                                       // using a temporary pointer
        last = otherQueue.last;
        copy(otherQueue.ptr, otherQueue.ptr + otherQueue.size, tempPtr);

        delete [] ptr;                                                  // Free up old memory

        ptr = tempPtr;                                                  // Point to the new memory block
        size = otherQueue.size;
    }
    return *this;
}


// ----------------------------------
// Queue operations
// ----------------------------------

/*
 * Make queue grow in size by 5 elements
 */
void Queue::growQueue(){

    Song *tmpPtr = ptr;             // Create temp array
    size += 5;
    ptr = new Song[size];           // Create new array with updated size

    for (int i=0; i<size-5;i++){    // Copy elements from temp array to updated array
        ptr[i] = tmpPtr[i];
    }
    delete [] tmpPtr;               // Delete temp array
}

/*
 * Add new element first in queue
 */
void Queue::enQueue(const Song &newSong){

    ptr[++last] = newSong;          // Increment last index by one and then add data

    if (isFull()) { growQueue(); }

    if (first == -1){               // If queue is empty
        first++;
    }
}

/*
 * Return first item in queue and update indexes
 * Output: Song object
 */
Song Queue::deQueue(){

    int tmpFirst = first++;     // tmpFirst will point to the element first pointed to before increment
    if (first > last){          // If first is larger than last the last item
        first = last = -1;      // was popped and queue indexes are reset
    }
    return ptr[tmpFirst];
}
