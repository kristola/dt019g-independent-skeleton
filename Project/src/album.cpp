/*
 * album.cpp
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Source file for class Album
 */

#include "../include/album.h"

using namespace std;
//using namespace std::placeholders;

// -------------------------------------------------------------------
// Printing
// -------------------------------------------------------------------

/*
 * Prints album
 * Input: printOption (enum) deciding whether to print album name only or full info
 */
void Album::printAlbum(const printOption &printingOption) const {

    cout << albumName << '\n';

    if (printingOption == titleOnly){ return; }

    else if (printingOption == fullAlbum){
        cout << '\n' << setw(56) << left << "Song name" << setw(30) << "Artist" << "Length\n";  // Headers for list

        int counter {1};
        for_each(albumSongs.begin(), albumSongs.end(),
                [&counter] (Song s) {                               // Lamda function which prints
                    cout << setw(2) << left << counter++ << ": ";   // song info and song number
                    s.printSong();
                });

        cout << '\n';
        cout << "==========================================================================================\n\n";
    }
}


// -------------------------------------------------------------------
// Manipulation
// -------------------------------------------------------------------

/*
 * Erases information in album
 */
void Album::clear(){

    albumName = "";
    albumSongs.clear();
}


// -------------------------------------------------------------------
// Overload operators
// -------------------------------------------------------------------

bool Album::operator<(const Album &alb) const{

    // Sums all songs in this and other and then compares
    songTime totalSongLengthA, totalSongLengthB;

    for (auto &song:albumSongs){
        totalSongLengthA = totalSongLengthA + song.getSongLength();
    }
    for (auto &song:alb.getAlbumSongs()) {
        totalSongLengthB = totalSongLengthB + song.getSongLength();
    }

    if (totalSongLengthA == totalSongLengthB){

    }
    return totalSongLengthA < totalSongLengthB;
}


// --------------------------------------------------------------------
// Overload << and >> for file handling
// --------------------------------------------------------------------

std::ostream &operator<<(std::ostream &os, const Album &album){

    // Sends Album and song info to output stream
    os << album.getAlbumName() << '\n';
    os << album.getAlbumSongs().size() << '\n';
    for (auto &song : album.getAlbumSongs()){
        os << song;
    }

    return os;
}

std::istream &operator>>(std::istream &is, Album &album){

    // Temporary variables to hold incoming data
    string tmpString;
    int numberOfSongs = 0;

    // Get album name
    getline(is, tmpString, '\n');
    tmpString.erase(remove(tmpString.begin(), tmpString.end(), '\r'));  // Remove potential '\r' before the '\n'
    album.setAlbumName(tmpString);

    // Get number of songs on album
    is >> numberOfSongs;
    if (is.get() == '\r') { is.get(); }     // If there is a '\r' before '\n' at the
                                            // end of each line, two characters are popped,
    // Add songs to album                   // otherwise only the '\n'
    Song tmpSong;
    for (int i=0; i<numberOfSongs;i++){
        is >> tmpSong;
        album.addSong(tmpSong);
    }

    return is;
}
