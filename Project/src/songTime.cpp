/*
 * songTime.cpp
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Source file for class songTime
 */

#include "songTime.h"
using std::ostream;
using std::istream;

// -------------------------------------
// Constructors
// -------------------------------------
songTime::songTime(int h, int m, int s){

    while (s >= 60){        // Ensures times are correctly stored
        s -= 60;
        m++;
    }

    while (m >= 60){
        m -= 60;
        h++;
    }

    hour = h;
    minute = m;
    seconds = s;
}

// -------------------------------------
// Overload operators
// -------------------------------------

// Overload =
songTime& songTime::operator=(const songTime &newSongTime){

    hour = newSongTime.getHour();
    minute = newSongTime.getMinute();
    seconds = newSongTime.getSeconds();

    return *this;
}

// Overload <
bool songTime::operator<(const songTime &newSongTime){

    if (hour != newSongTime.getHour()){
        return hour < newSongTime.getHour();
    }

    else if (minute != newSongTime.getMinute()){
        return minute < newSongTime.getMinute();
    }

    else return seconds < newSongTime.getSeconds();

}

// Overload +
songTime& songTime::operator+(const songTime &newSongTime){

    hour += newSongTime.getHour();
    minute += newSongTime.getMinute();
    seconds += newSongTime.getSeconds();

    while (seconds >= 60){      // Ensures new values
        seconds -= 60;          // conform to time format
        minute++;
    }
    while (minute >= 60){
        minute -= 60;
        hour++;
    }

    return *this;
}

// Overload ==
bool songTime::operator==(const songTime &newSongTime){

    return (hour == newSongTime.getHour()
            && minute == newSongTime.getMinute()
            && seconds == newSongTime.getSeconds());
}


// --------------------------------------------------
// Overload << and >> for file handling purposes
// --------------------------------------------------

// Overload <<
ostream &operator<<(ostream &os, const songTime &songTime){

    // Converts to hours and minutes to seconds and passes to output stream
    os << songTime.getHour()*3600 + songTime.getMinute()*60 + songTime.getSeconds();
    return os;
}

// Overload >>
istream &operator>>(istream &is, songTime &songTime){

    // Read from input stream
    int totSeconds = 0, hours, minutes;
    is >> totSeconds;
    if (is.get() == '\r') { is.get(); }                 // If there is a '\r' before '\n' at
                                                        // end of each line, two characters are popped,
    hours = totSeconds/3600;    // Converts seconds     // otherwise only the '\n'
    totSeconds %= 3600;         // to format hh:mm:ss
    minutes = totSeconds/60;
    totSeconds %= 60;

    // Set member data
    songTime.setHour(hours);
    songTime.setMinute(minutes);
    songTime.setSecond(totSeconds);

    return is;
}