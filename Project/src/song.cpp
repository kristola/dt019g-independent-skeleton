/*
 * song.cpp
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Source file for class Song
 */

#include "../include/song.h"
using namespace std;


// ------------------------------------------------------
// Print functions
// ------------------------------------------------------


/*
 * Prints song
 */
void Song::printSong() const {

    cout << setw(50) << left << songName << '\t';
    cout << setw(20) << left << songArtist << '\t';

    string tmpTime, tmpMinute, tmpSeconds;

    if (songLength.getHour() > 0){

        // Converts ints to strings and ensures correct formatting
        tmpMinute = to_string(songLength.getMinute());
        if (tmpMinute.size() == 1) { tmpMinute.insert(0, 1, '0'); }
        tmpSeconds = to_string(songLength.getSeconds());
        if (tmpSeconds.size() == 1) { tmpSeconds.insert(0, 1,'0'); }

        tmpTime = to_string(songLength.getHour()) + ':' + tmpMinute + ':' + tmpSeconds;
    }

    else if (songLength.getHour() >= 0) {

        // Converts ints to strings and ensures correct formatting
        tmpSeconds = to_string(songLength.getSeconds());
        if (tmpSeconds.size() == 1) { tmpSeconds.insert(0, 1,'0'); }

        tmpTime = to_string(songLength.getMinute()) + ':' + tmpSeconds;
    }

    cout << setw(10) << right << tmpTime << '\n';
}

// ------------------------------------------------------
// Overload operators
// ------------------------------------------------------

Song &Song::operator=(const Song &s){

    songName = s.getSongName();
    songArtist = s.getSongArtist();
    songLength = s.getSongLength();

    return *this;
}


// -------------------------------------------------------
// Overload << and >> for file handling
// -------------------------------------------------------
std::ostream &operator<<(std::ostream &os, const Song &song){

    // Sends member data to output stream
    os << song.getSongName() << DELIM;
    os << song.getSongArtist() << DELIM;
    os << song.getSongLength() << '\n';

    return os;
}

std::istream &operator>>(std::istream &is, Song &song){

    // Read data
    string tmpString;
    getline(is, tmpString, DELIM);
    song.setName(tmpString);
    getline(is, tmpString, DELIM);
    song.setArtist(tmpString);

    songTime tmpSongTime;
    is >> tmpSongTime;
    song.setSongTime(tmpSongTime);

    return is;
}
