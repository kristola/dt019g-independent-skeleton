//
// Created by Isak Kristola on 2019-02-25.
//

#ifndef DT019G_MENU_H
#define DT019G_MENU_H

#include <vector>
#include <string>
#include <iostream>
#include "menuItem.h"

class Menu {
private:
    // Member data
    std::vector<MenuItem> menuItems;
    std::string header;

public:
    // Constructors
    Menu()=default;
    explicit Menu(std::string header) : header{std::move(header)} {};

    // Get & set
    void setMenuHeader(const std::string &newHeader) { header = newHeader; }

    // Member functions
    void addItem(const std::string &option, const bool &enabled);
    void printMenuItems();
    int getMenuChoice();
    void enableFullMenu();

};


#endif //DT019G_MENU_H
