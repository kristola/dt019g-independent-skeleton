/*
 * queue.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for class Queue
 */

#ifndef DT019G_QUEUE_H
#define DT019G_QUEUE_H

#include <iostream>
#include "song.h"

class Queue {
private:
    Song *ptr;
    size_t size;
    int first, last;

public:
    // Constructors & destructor
    Queue();                            // Default
    Queue(const Queue &otherQueue);     // Copy
    ~Queue();                           // Destructor

    // Overloaded operators
    Queue& operator= (const Queue &otherQueue);

    // Queue status methods
    bool isFull() { return last+1 == size; };       // +1 since index starts at 0
    bool isEmpty() { return first == -1; };         // first and last are set to -1 by default
                                                    // and when last element is dequeued
    // Queue operations
    void enQueue(const Song &newSong);
    Song deQueue();
    void growQueue();
};


#endif //DT019G_QUEUE_H
