/*
 * jukebox.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for class Jukebox
 */

#ifndef DT019G_JUKEBOX_H
#define DT019G_JUKEBOX_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <random>
#include <limits>
#include <algorithm>
#include <functional>
#include <thread>       // For waiting between prints
#include <chrono>       //          -||-
#include "queue.h"
#include "album.h"
#include "menu.h"
#include "constants.h"

class Jukebox {
private:
    std::vector<Album> albumList;
    Queue playlist;
    Menu mainMenu, fileMenu, printMenu, playMenu;

    // Main menu functions
    void file();
    void print();
    void play();
    void pressEnterToContinue();

    // Play menu functions
    void createPlaylist(const generateOption &generateOption);
    void printNumberedList(const std::vector<Song> &songList);
    std::vector<int> getUserSongChoices();
    void playPlaylist();

    // Search
    auto findAlbum(std::string targetAlbum);    // Passed by value since string param. will be manipulated

    // Print
    void printAll(const sortOption &sortOption, const printOption &printOption);
    void printOneAlbum();

    // Album handling
    void removeAlbum();
    void addAlbum();

    // Sort
    void sortAlbumList(const sortOption &sortOption);

    // File handling
    bool OpenFromFile();
    bool saveToFile();


public:
    // Constructors
    Jukebox();

    // Member functions
    void run();
};


#endif //DT019G_JUKEBOX_H
