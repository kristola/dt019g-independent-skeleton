/*
 * constants.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for constants used in Project
 */

#ifndef DT019G_CONSTANTS_H
#define DT019G_CONSTANTS_H

#include <string>

// Delimiter for reading and writing to file
const char DELIM = '|';

// File used for file I/O
const std::string FILE_NAME {"jukebox.txt"};

// Enums providing function options
enum sortOption {byName, byLength};
enum printOption {fullAlbum, titleOnly};
enum generateOption {randomize, byUserInput};

#endif //DT019G_CONSTANTS_H
