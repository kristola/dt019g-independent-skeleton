/*
 * songTime.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for class songTime
 */


#ifndef DT019G_TIME_H
#define DT019G_TIME_H

#include <iostream>
#include <string>
#include <fstream>
#include "constants.h"

class songTime {
private:
    int hour;
    int minute;
    int seconds;

public:
    // Constructors
    songTime() : hour{0}, minute{0}, seconds{0} {} ;
    songTime(int h, int m, int s);

    // Get and set
    int getHour() const { return hour; }
    int getMinute() const { return minute; }
    int getSeconds() const { return seconds; }
    void setHour(int &h) { hour = h; }
    void setMinute(int &m) { minute = m; }
    void setSecond(int &s) { seconds = s; }

    // Overload operators
    songTime& operator=(const songTime &newSongTime);
    bool operator<(const songTime &newSongTime);
    songTime& operator+(const songTime &newSongTime);
    bool operator==(const songTime &newSongTime);
};

// Overload << and >> for file handling
std::ostream &operator<<(std::ostream &os, const songTime &songTime);
std::istream &operator>>(std::istream &is, songTime &songTime);


#endif //DT019G_TIME_H
