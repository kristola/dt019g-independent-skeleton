/*
 * song.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for class Song
 */

#ifndef DT019G_SONG_H
#define DT019G_SONG_H

#include <string>
#include <iomanip>
#include "songTime.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <algorithm>
#include <functional>

class Song {
private:
    std::string songName;
    std::string songArtist;
    songTime songLength;

public:
    // Constructors
    Song()=default;
    Song(std::string sname, std::string sart, const songTime &stim)
            : songName{std::move(sname)}, songArtist{std::move(sart)}, songLength{stim} {};

    // Get and set
    void setName(const std::string &newName) { songName = newName; }
    void setArtist(const std::string &newArtist) { songArtist = newArtist; }
    void setSongTime(const songTime &newSongTime) { songLength = newSongTime; }
    std::string getSongName() const { return songName; }
    std::string getSongArtist() const { return songArtist; }
    songTime getSongLength() const { return songLength; }

    // Print functions
    void printSong() const;

    // Overload operators
    Song &operator=(const Song &s);
};

// Overload << and >> for file handling
std::ostream &operator<<(std::ostream &os, const Song &song);
std::istream &operator>>(std::istream &is, Song &song);


#endif //DT019G_SONG_H
