/*
 * album.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for class Album
 */

#ifndef DT019G_ALBUM_H
#define DT019G_ALBUM_H

#include "song.h"
#include "constants.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <limits>
#include <algorithm>
#include <functional>

class Album {
private:
    std::string albumName;
    std::vector<Song> albumSongs;

public:
    Album()=default;
    explicit Album(std::string aName) : albumName{std::move(aName)} {};

    // Get and set
    void setAlbumName(const std::string &newAlbumName) { albumName = newAlbumName; }
    std::string getAlbumName() const { return albumName; }
    std::vector<Song> getAlbumSongs() const { return albumSongs; }

    // Printing
    void printAlbum(const printOption &opt) const;

    // Manipulation
    void addSong(const Song &newSong) { albumSongs.push_back(newSong); }
    void clear();

    // Overload operators
    bool operator<(const Album &alb) const;
};

// Overload << and >> for file handling
std::ostream &operator<<(std::ostream &os, const Album &album);
std::istream &operator>>(std::istream &is, Album &album);


#endif //DT019G_ALBUM_H
