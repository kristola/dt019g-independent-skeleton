/*
 * menuItem.h
 * Projekt
 * Objektbaserad programmering i C++, VT19
 * Created by Isak Kristola on 2019-02-25.
 *
 * Header file for class MenuItem (fully implemented in the header, no source file supplied)
 */

#ifndef DT019G_MENUITEM_H
#define DT019G_MENUITEM_H

#include <string>

class MenuItem {
private:
    std::string menuText;
    bool enabled {false};

public:
    // Constructors
    MenuItem()=default;
    MenuItem(std::string option, const bool &enabled) : menuText{std::move(option)}, enabled(enabled) {};

    // Get & set
    std::string getMenuItem() const { return menuText; }
    void enable() { enabled = true; }
    bool isEnabled() const { return enabled; }
};


#endif //DT019G_MENUITEM_H
