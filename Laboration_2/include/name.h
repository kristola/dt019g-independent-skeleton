/* name.h
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 1
 * Isak Kristola, iskr1300
 *
 * Header file for name class
 */

#ifndef _laboration_2_include_name_h_
#define _laboration_2_include_name_h_

#include <iostream>
#include <string>

class Name{
private:
    std::string firstName;
    std::string lastName;

public:
    // Constructors
    Name();                                                 // Default constructor
    Name(std::string pFirstName, std::string pLastName);    // Initializing constructor

    // Put values in data members
    void setFirstName();
    void setLastName();

    // Get member data
    std::string getFirstName() const { return firstName ;}
    std::string getLastName() const { return lastName ;}
};

#endif
