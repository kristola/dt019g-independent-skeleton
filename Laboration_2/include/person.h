/* person.h
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 3
 * Isak Kristola, iskr1300
 *
 * Header file for Person class and free funtions
 */

#ifndef _laboration_2_include_person_h_
#define _laboration_2_include_person_h_

#include <iostream>
#include <string>
#include <vector>
#include <limits>
#include <iomanip>
#include "name.h"
#include "address.h"

class Person{
private:
    Name name;
    Address address;
    std::string persNr;
    int shoeNr;
public:
    // Constructors
    Person();                                                                       // Default constructor
    Person(Name aName, Address anAddress, std::string aPersNr, int aShoeNr);        // Initializing constructor

    // Getters
    void printData();

};

void newPerson(std::vector<Person> &persVec);
void printPersons(const std::vector<Person> &persVec);
int menu(const std::string menuItems[], const int &ITEM_NUM);

#endif
