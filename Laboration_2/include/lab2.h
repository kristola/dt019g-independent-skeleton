#ifndef _laboration_2_include_lab2_h_
#define _laboration_2_include_lab2_h_

#include "person.h"
#include <string>
#include <iostream>
#include <vector>

std::string getAssignmentInfo() { return "Laboration 2!"; }

void newPerson(std::vector<Person> &persVec);
void printPersons(const std::vector<Person> &persVec);

#endif
