/* address.h
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 2
 * Isak Kristola, iskr1300
 *
 * Header file for address class
 */

#ifndef _laboration_2_include_address_h_
#define _laboration_2_include_address_h_

#include <iostream>
#include <string>

class Address{
private:
    std::string streetAddress;
    std::string postalNr;
    std::string city;

public:
    // Constructors
    Address();                                                          // Default constructor
    Address(std::string street, std::string postal, std::string city);  // Initializing constructor

    // Setters
    void setStreetAddress();
    void setPostalNr();
    void setCity();

    // Getters
    std::string getStreetAddress() const { return streetAddress; };
    std::string getPostalNr() const { return postalNr; };
    std::string getCity() const { return city; };

};

#endif
