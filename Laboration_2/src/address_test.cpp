/* address_test.cpp
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 2
 * Isak Kristola, iskr1300
 *
 * Program that tests address class
 */

#include "address.h"
using namespace std;

/**
 * Main program
 */
int main()
{
    cout << "Laboration 2, test program for class Address" << endl;

    // Test default constructor
    cout << "Creating object using default constructor. Object name: \"addressTest1\"" << endl << endl;
    Address addressTest1;

    // Test setters
    cout << "Testing set functions on addressTest1. " << endl;
    addressTest1.setStreetAddress();
    addressTest1.setPostalNr();
    addressTest1.setCity();

    // Test getters
    cout << endl << "Testing get functions on addressTest1." << endl;
    cout << "addressTest1.getStreetAddress(): " << addressTest1.getStreetAddress() << endl;
    cout << "addressTest1.getPostalNr(): " << addressTest1.getPostalNr() << endl;
    cout << "addressTest1.getCity(): " << addressTest1.getCity() << endl << endl;

    // Tests initializing constructor
    cout << "Initializing a address object with street address, postal nr, and city as parameters:" << endl;
    cout << "Address addressTest2(\"Skolgatan 71\", \"267 45\", \"Stroustrupville\")" << endl << endl;
    Address addressTest2("Skolgatan 71", "267 45", "Stroustrupville");

    // Test getters
    cout << "Testing get functions on addressTest2." << endl;
    cout << "addressTest2.getStreetAddress(): " << addressTest2.getStreetAddress() << endl;
    cout << "addressTest2.getPostalNr(): " << addressTest2.getPostalNr() << endl;
    cout << "addressTest2.getCity(): " << addressTest2.getCity() << endl << endl;

    return 0;
}

