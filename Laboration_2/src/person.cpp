/* person.cpp
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 3
 * Isak Kristola, iskr1300
 *
 * Source file for Person class and free funtions
 */

#include "person.h"
using namespace std;

Person::Person(){
    persNr = "";
    shoeNr = 0;
}

Person::Person(Name aName, Address anAddress, string aPersNr, int aShoeNr){
    name = aName;
    address = anAddress;
    persNr = aPersNr;
    shoeNr = aShoeNr;
}

void Person::printData(){

    // Print formatted member data
    cout << setw(15) << left << "Name: " << name.getFirstName() << " " << name.getLastName() << '\n';
    cout << setw(15) << left << "Address: " << address.getStreetAddress() << '\n';
    cout << setw(15) << left << "Postcode:" << address.getPostalNr() << " " << address.getCity() << '\n';
    cout << setw(15) << left << "Person number: " << persNr << '\n';
    cout << setw(15) << left << "Shoe size: " << shoeNr << "\n\n";
};

void newPerson(std::vector<Person> &persVec){

    // Ask user for input and make temp objects
    cout << "** Add person **\n";
    Name tmpName;                   // Create temp Name object and set member data
    tmpName.setFirstName();
    tmpName.setLastName();
    Address tmpAddress;             // Create temp Address object and set member data
    tmpAddress.setStreetAddress();
    tmpAddress.setPostalNr();
    tmpAddress.setCity();
    string pNr;                     // Get person number and shoesize
    int shoe;
    cout << "Person number: ";
    getline(cin, pNr);
    cout << "Shoe size: ";
    cin >> shoe;
    cin.clear();                                            // Clear potential failed cin state
    cin.ignore(numeric_limits<streamsize>::max(), '\n');    // Flush buffer, remove newline left in stream

    // Create Person object and add to person vector
    Person tmpPerson(tmpName, tmpAddress, pNr, shoe);
    persVec.push_back(tmpPerson);
}

void printPersons(const std::vector<Person> &persVec){

    // Iterates through person vector and calls member functions which prints data
    cout << "** Printing persons in list **\n\n";
    for (auto e : persVec){
        e.printData();
    }
}

int menu(const std::string menuItems[], const int &ITEM_NUM){

    // Prints menu
    cout << "** MENU **\n";
    for (int i=0; i<ITEM_NUM; i++){
        cout << menuItems[i] << '\n';
    }

    // Gets user choice
    cout << "Enter your choice and press ENTER: ";
    char choice;
    cin >> choice;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');    // Flush buffer, remove newline left in stream
    return choice;
}
