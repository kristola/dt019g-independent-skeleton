/* lab2.cpp
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 3
 * Isak Kristola, iskr1300
 *
 * Program that creates an interactive menu which
 * uses and tests person class
 */

#include "person.h"

/**
 * Main program
 */

int main()
{
    std::vector<Person> personList;

    // Data arrays for initial entries in person vector
    const std::string names[3][2] {"Isak", "Kristola",
                            "Jonas", "Knutsson",
                            "Felis", "Eyjah"};
    const std::string addresses[3][3] {"Sofielundsvagen 71", "214 34", "Malmo",
                              "Bergamottvagen 88", "666 66", "Bro",
                              "Root street 345", "543 56", "Kingston"};
    const std::string persNrs[3] {"880215-6453","580245-2345","991231-1245"};
    int shoeSizes[3] {43,37,87};

    // Fetches information from data arrays and creates Person objects with it
    for (int i=0; i<3; i++) {
        Name tempName(names[i][0], names[i][1]);                                // First creates temporary
        Address tempAddr(addresses[i][0], addresses[i][1], addresses[i][2]);    // Name and Address objects
        Person tempPerson{tempName, tempAddr, persNrs[i], shoeSizes[i]};
        personList.push_back(tempPerson);
    }

    // Initialize menu
    const int ITEM_NUM = 3;
    std::string menuItems[ITEM_NUM] = {"1. Add new person to list",
                                       "2. Print list",
                                       "3. Quit"};

    // Run menu
    bool run = true;
    do
    {
        switch(menu(menuItems, ITEM_NUM))
        {
            case '1': newPerson(personList);
            break;
            case '2': printPersons(personList);
            break;
            case '3': run = false;
            default:
            break;
        };
    } while(run);

    return 0;
}
