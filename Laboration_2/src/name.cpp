/* name.cpp
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 1
 * Isak Kristola, iskr1300
 *
 * Source file for name class
 */
#include "name.h"
using std::string;
using std::cin;
using std::cout;


Name::Name() {
    // Default constructor
    firstName = "";
    lastName = "";
}

Name::Name(string fName, string lName) {
    // Initializing contructor that takes two strings as parameters
    firstName = fName;
    lastName = lName;
}

bool Name::operator==(const Name &name) const{
    return (firstName == name.firstName && lastName == name.lastName);
}

//bool Name::operator<(const Name &name const)
//{
//
//}

void Name::setFirstName() {
    // Gets input and sets member variable for first name
    string pFirstName;
    cout << "First name: ";
    getline(cin,pFirstName);
    firstName = pFirstName;
}

void Name::setLastName() {
    // Gets input and sets member variable for last name
    string pLastName;
    cout << "Last name: ";
    getline(cin, pLastName);
    lastName = pLastName;
}

