/* address.cpp
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 2
 * Isak Kristola, iskr1300
 *
 * Source file for Address class
 */

#include "address.h"
using std::string;
using std::cout;
using std::cin;

Address::Address() {
    // Default constructor
    streetAddress = "";
    postalNr = "";
    city = "";
}

Address::Address(string aStreet, string aPostal, string aCity) {
    // Initializing constructor that takes three strings as parameters
    streetAddress = aStreet;
    postalNr = aPostal;
    city = aCity;
}

void Address::setStreetAddress(){
    // Gets input and sets member variable for street address
    string tStreet;
    cout << "Street address: ";
    std::getline(cin, tStreet);
    streetAddress = tStreet;
}

void Address::setPostalNr(){
    // Gets input and sets member variable for postal nr
    string tPostal;
    cout << "Postal nr: ";
    std::getline(cin, tPostal);
    postalNr = tPostal;
}

void Address::setCity(){
    // Gets input and sets member variable for city
    string tCity;
    cout << "City: ";
    std::getline(cin, tCity);
    city = tCity;
}
