/* name_test.cpp
 * Obektbaserad programmering i C++, VT19
 * Laboration 2, Assignment 1
 * Isak Kristola, iskr1300
 *
 * Program that tests name class
 */

#include "name.h"
using namespace std;

/**
 * Main program
 */

int main()
{
    cout << "Laboration 2, nameTest1 program for class Name" << endl << endl;

    // Test default constructor
    cout << "Creating object using default constructor. Object name: \"nameTest1\"" << endl << endl;
    Name nameTest1;

    // Test setters
    cout << "Testing set functions on object nameTest1. " << endl;
    nameTest1.setFirstName();
    nameTest1.setLastName();

    // Test getters
    cout << endl << "Testing get functions on object nameTest1. " << endl;
    cout << "nameTest1.getFirstName(): " << nameTest1.getFirstName() << endl;
    cout << "nameTest1.getLastName(): " << nameTest1.getLastName() << endl << endl;

    // Tests initializing constructor
    cout << "Initializing a name object with first name and last name as parameters: ";
    cout << "Name nameTest2(\"Isak\",\"Kristola\")" << endl << endl;
    Name nameTest2("Isak", "Kristola");

    // Test getters
    cout << "Testing get functions on object nameTest2. " << endl;
    cout << "nameTest2.getFirstName(): " << nameTest2.getFirstName() << endl;
    cout << "nameTest2.getLastName(): " << nameTest2.getLastName() << endl;

    return 0;
}
