// SortingTest.cpp
// Laboration 4, sorting algorithms
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Source file for SortingTest class

#include "SortingTest.h"

using namespace std;


SortingTest::SortingTest(const int &arrSize){
    // Initializing arrays with specific size

    array = IntArray(arrSize);
}

void SortingTest::runTests(){

    cout << endl << "\nRunning tests on array with " << array.getMaxSize()
                    << " numbers!" << '\n' << "All times in seconds";

    double timeInMicroseconds = 0;

    // Bubble
    for (int i=1; i<=10; i++) {
        cout << "\nBubble #" << i << ": ";
        array.fillWithNumbers();
        timer.start();
        array.bubbleSort();
        timeInMicroseconds = timer.stop();
        cout << setw(14) << left << timeInMicroseconds / 1000000;
        bubbleTime.push_back(timeInMicroseconds);
    }

    // Insert
    for (int j=1; j<=10; j++) {
        cout << "\nInsert #" << j << ": ";
        array.fillWithNumbers();
        timer.start();
        array.insertSort();
        timeInMicroseconds = timer.stop();
        cout << setw(14) << left << timeInMicroseconds / 1000000;
        insertTime.push_back(timeInMicroseconds);
    }

    // Select
    for (int k=1; k<=10; k++) {
        cout << "\nSelect #" << k << ": ";
        array.fillWithNumbers();
        timer.start();
        array.selectSort();
        timeInMicroseconds = timer.stop();
        cout << setw(14) << left << timeInMicroseconds / 1000000;
        selectTime.push_back(timeInMicroseconds);
    }

    // Quick
    for (int l=1; l<=10; l++) {
        cout << "\nQuick #" << l << ": ";
        array.fillWithNumbers();
        timer.start();
        array.quickWrapper();
        timeInMicroseconds = timer.stop();
        cout << setw(14) << left << timeInMicroseconds / 1000000;
        quickTime.push_back(timeInMicroseconds);
    }
}

void SortingTest::saveToFile() {

    fstream outFile("results.txt", ios::app);           // ios::app appends data to file if not empty

    // Bubble
    double totalMicrosecs = 0;
    for (auto e : bubbleTime){
        totalMicrosecs += e;
    }
    outFile << "bubble" << '\t' << array.getSize() << '\t' << totalMicrosecs/10/1000000 << '\n';

    // Select
    totalMicrosecs = 0;
    for (auto e : selectTime){
        totalMicrosecs += e;
    }
    outFile << "select" << '\t' << array.getSize() << '\t' << totalMicrosecs/10/1000000 << '\n';

    // Insert
    totalMicrosecs = 0;
    for (auto e : insertTime){
        totalMicrosecs += e;
    }
    outFile << "insert" << '\t' << array.getSize() << '\t' << totalMicrosecs/10/1000000 << '\n';

    // Quick
    totalMicrosecs = 0;
    for (auto e : quickTime){
        totalMicrosecs += e;
    }
    outFile << "quick" << '\t' << array.getSize() << '\t' << totalMicrosecs/10/1000000 << '\n';

    outFile.close();
}

void SortingTest::printResults(){

    // Bubble
    double totalMilliseconds = 0;
    cout << "Bubblesort " << array.getMaxSize() << " numbers: ";
    for (int i=0; i<bubbleTime.size();i++){
        totalMilliseconds += bubbleTime[i];
    }
    cout << totalMilliseconds/bubbleTime.size()/1000000 << "\n";

    // Insert
    totalMilliseconds = 0;
    cout << "Insertsort " << array.getMaxSize() << " numbers: ";
    for (int i=0; i<insertTime.size();i++){
        totalMilliseconds += insertTime[i];
    }
    cout << totalMilliseconds/insertTime.size()/1000000 << "\n";

    // Select
    totalMilliseconds = 0;
    cout << "Selectsort " << array.getMaxSize() << " numbers: ";
    for (int i=0; i<selectTime.size();i++){
        totalMilliseconds += selectTime[i];
    }
    cout << totalMilliseconds/selectTime.size()/1000000 << "\n";

    // Quick
    totalMilliseconds = 0;
    cout << "Quicksort " << array.getMaxSize() << " numbers:  ";
    for (int i=0; i<quickTime.size();i++){
        totalMilliseconds += quickTime[i];
    }
    cout << totalMilliseconds/quickTime.size()/1000000 << "\n";
}

void SortingTest::printNumbersInArray(){
    int counter = 0;
    for (int i=0; i<array.getSize(); i++){
        cout << setw(6) << left << array.getValue(i);
        counter++;
        if (counter % 50 == 0) cout << endl;
    }
}
