// intArray.cpp
// Laboration 4, sorting algorithms
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Source file fir IntArray class

#include "../include/intArray.h"
using namespace std;

// Default constructor
IntArray::IntArray(): maxSize(0), size(0){
    arr = nullptr;
}

// Initializing constructor
IntArray::IntArray(int pMaxSize): maxSize(pMaxSize), size(0){
    arr = new int[maxSize];
}

// Copy constructor
IntArray::IntArray(const IntArray &a): maxSize(a.maxSize), size(a.size){
    arr = new int[a.maxSize];
    for (int i=0; i<a.size; i++)
        arr[i] = a.arr[i];
}

// Destructor
IntArray::~IntArray(){
    delete [] arr;
    arr = NULL;
}

// Sorting functions
void IntArray::bubbleSort(){

    for(int pass=0; pass < size; pass++) {
        for(int i=0; i < size; i++){
            if(getValue(i) < getValue(i+1)){
                swap(arr[i], arr[i+1]);
            }
        }
    }
}

void IntArray::selectSort(){

    int smallIdx = 0;
    for (int i=0; i < size-1; i++){
        smallIdx = i;                           // Index for smallest element to the right of pos i
        for (int j=i+1; j < size; j++){         // Search for smallest "unsorted" element
            if (arr[j] < arr[smallIdx]){
                smallIdx = j;
            }
        }
        if (smallIdx != i){
            swap(arr[i], arr[smallIdx]);
        }
    }
}

void IntArray::insertSort(){

    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < size-1; i++)
    {
        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i+1; j < size; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;

        // Swap the found minimum element with the first element
        swap(arr[min_idx], arr[i]);
    }
}

void IntArray::quickWrapper(){
    // Wrapper function for recursive quicksort

    quickSort(0, static_cast<int>(size)-1);
}

void IntArray::quickSort(int first, int last){

    if(first < last){
        int low = first;
        int high = last;
        if(arr[first] > arr[last]){
            swap(arr[first], arr[last]);
        }

        do{
            // Start from beginning of array and search first value larger than a[first]
            do{ low++; } while(arr[low] < arr[first]);

            // Start from end of array and search first value smaller than a[first]
            do{ high--; } while(arr[high] > arr[first]);

            // Change place of a[low] and a[high] if low < high
            if(low < high)
                swap(arr[low], arr[high]);
        } while(low <= high);

        swap(arr[first], arr[high]);

        quickSort(first, high-1);
        quickSort(high+1, last);
    }
}

void IntArray::swap(int &a, int &b){

    int tmp = a;
    a = b;
    b= tmp;
}

// Get value from element with index idx
int IntArray::getValue(int idx) const{
    return arr[idx];
}

// Set value at indec idx
bool IntArray::setValue(int idx, int value){

    bool valueSet = false;
    if (idx < maxSize){
        arr[idx] = value;
        valueSet = true;
    }
    return valueSet;
}

// Add integer at first free slot in array
// Checks that there is an available slot
bool IntArray::addValue(int value){
    bool valueAdded = false;
    if (size < maxSize){
        arr[size] = value;
        size++;
        valueAdded = true;
    }
    return valueAdded;
}

void IntArray::fillWithNumbers(){

    // Initialize random engine
    default_random_engine generator(static_cast<unsigned>(time1(0)));
    uniform_int_distribution<int> random(0,static_cast<int>(maxSize-1));

    // Generate numbers
    if (size == 0) {                         // If array is empty
        for (int i = 0; i < maxSize; i++) {
            addValue(random(generator));
        }
    }

    else{                                   // If array already has numbers
        for (int i = 0; i < size; i++) {
            setValue(i, random(generator));
        }
    }
}

// Overload assignment operator
const IntArray &IntArray::operator=(const IntArray &a){
    if(this != &a){
        delete []arr;
        arr = new int[a.maxSize];
        maxSize = a.maxSize;
        size = a.size;
        for (int i=0; i < a.size; i++){
            arr[i] = a.arr[i];
        }
    }
    return *this;
}
