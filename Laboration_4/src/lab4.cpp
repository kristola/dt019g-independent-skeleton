// lab4.cpp
// Laboration 4, sorting algorithms
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Main file

#include <iostream>
#include "SortingTest.h"

int main()
{

    // Creates test ojects and adds to vector
    SortingTest test5k(5000), test10k(10000), test15k(15000), test20k(20000), test25k(25000),
                test30k(30000), test35k(35000), test40k(40000);
    std::vector<SortingTest> tests { test5k, test10k, test15k, test20k, test25k, test30k, test35k, test40k };

    // Run tests and save to file
    for (auto &e : tests){
        e.runTests();
        e.saveToFile();
    }

    // Print results
    std::cout << "\n\n***** RESULTS *****\n\nAverage of 10 sortings per array size and sort type.\n";
    std::cout << "All results in seconds.\n\n";
    for (auto &e : tests){
        e.printResults();
    }

    return 0;
}
