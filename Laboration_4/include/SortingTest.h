// SortingTest.h
// Laboration 4, sorting algorithms
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Header for SortingTest class

#ifndef DT019G_USERINTERFACE_H
#define DT019G_USERINTERFACE_H

#include <random>
#include <ctime>
#include <iomanip>
#include <fstream>
#include <string>
#include "intArray.h"
#include "timer.h"

// Class that creates arrays, runs tests, and records results
class SortingTest {
private:
    // Data
    IntArray array;
    std::vector<double> bubbleTime, quickTime, insertTime, selectTime;
    Timer timer;

public:
    // Constructors
    SortingTest() = default;
    SortingTest(const int &arrSize);

    // File handling
    void saveToFile();

    // Other functions
    void runTests();
    void printNumbersInArray();
    void printResults();
};

#endif //DT019G_USERINTERFACE_H
