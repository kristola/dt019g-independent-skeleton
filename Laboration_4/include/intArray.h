// intArray.h
// Laboration 4, sorting algorithms
// Objektbaserad programmering i C++, VT2019
// Isak Kristola, iskr1300
//
// Header file for intArray class

#ifndef DT019G_INTARRAY_H
#define DT019G_INTARRAY_H

#include <iostream>
#include <random>

class IntArray {
private:
    int* arr;           // Pointer to array
    size_t maxSize;     // Max size of array
    size_t size;        // Array actual size

public:
    // Constructors and destructor
    IntArray();
    IntArray (int pNum);
    IntArray (const IntArray &a);
    ~IntArray();

    // Sorting functions
    void bubbleSort();
    void insertSort();
    void selectSort();
    void quickSort(int first, int last);
    void quickWrapper();

    void swap(int &a, int &b);

    // Getters and setters
    size_t getMaxSize() const { return maxSize; }
    size_t getSize() const { return size; }
    int getValue (int idx) const;
    bool setValue (int idx, int value);
    bool addValue(int value);

    // Overload assignment operator
    const IntArray &operator=(const IntArray &a);

    // Other functions
    void fillWithNumbers();

};

#endif //DT019G_INTARRAY_H
