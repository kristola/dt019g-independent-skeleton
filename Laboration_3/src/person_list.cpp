// person_list.cpp
// Source file for class PersonList
// Isak Kristola
// Objektbaserad programmering i C++, VT2019
// Laboration 3, Operator Overloading

#include "../include/person_list.h"


// ---------------------------
// Get person from Person list
// ---------------------------
bool PersonList::getPerson(int idx, Person &p) const {

    bool idxOk = false;                         // Returns false if index is not in Person list
    if (idx > -1 && idx < int(pList.size())){
        p = pList[idx];                         // Sets referenced input Person to Person in list
        idxOk = true;                           // at index idx
    }
    return idxOk;
}

// -------------------
// Save to file
// -------------------
void PersonList::saveToFile() {

    std::ofstream of(fileName, std::ios::out);  // Create output stream object
    for(auto &e: pList){
        of << e << '\n';                        // Uses overloaded <<
    }
    of.close();
}

void PersonList::readFromFile() {

    std::ifstream inf(fileName, std::ios::in);  // Create input stream object
    pList.clear();                              // Erases prev. data in pList
    Person tmpPerson;
    while(inf >> tmpPerson){                    // Continues as long as there is data in inf
        pList.push_back(tmpPerson);
    }

    inf.close();
}

// --------------------
// Comparison functions
// --------------------
void PersonList::sortByName(){
    sort(pList.begin(), pList.end(), nameCmp);
}

void PersonList::sortByPn(){
    sort(pList.begin(), pList.end(), persNrCmp);
}

void PersonList::sortByShoe() {
    sort(pList.begin(), pList.end(), shoeCmp);
}

// ---------------------------
// Comparison helper functions
// ---------------------------
bool nameCmp(const Person &p1, const Person &p2){
    return p1.getName() < p2.getName();
}

bool persNrCmp(const Person &p1, const Person &p2){
    return p1.getPersNr() < p2.getPersNr();
}

bool shoeCmp(const Person &p1, const Person &p2){
    return p1.getShoeNr() < p2.getShoeNr();
}
