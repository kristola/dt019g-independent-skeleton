// lab3.cpp
// Main file
// 
// Isak Kristola, iskr1300
// Objektbaserad programmering i C++, VT19
// Laboration 3, Operator Overloading


#include "UserInterface.h"

int main()
{
    // Set up UI that handles interactions with user
    UserInterface userinterface;
    userinterface.run();

    return 0;
}
