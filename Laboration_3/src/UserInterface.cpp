// user_interface.cpp
// Source file for UserInterface
//
// Isak Kristola, iskr1300
// Objektbaserad programmering i C++, VT2019
// Laboration 3, Operator Overloading

#include "UserInterface.h"
#include "person.h"

using namespace std;

// -----------------
// Main loop
// -----------------
void UserInterface::run(){

    // Program loop
    bool run;
    do {
        run = runMenu();
        cout << "\nPress ENTER to continue";        // Prompts user before "returning" to menu
        cin.get();
    } while (run);
}

// -----------------
// Menu function
// -----------------
bool UserInterface::runMenu(){

    printMenuOptions();

    int userChoice;
    cin >> userChoice;
    cin.get();

    // Calls functions based on user input
    switch(userChoice) {
        case 1:
            add();
            break;
        case 2:
            printPersonList();
            break;
        case 3:
            save();
            break;
        case 4:
            read();
            break;
        case 5:
            sortName();
            break;
        case 6:
            sortPn();
            break;
        case 7:
            sortShoe();
            break;
        case 8:
            return false;
        default:
            std::cout << "Bad input, try again..\n";
            cin.clear();                                            // Flush and clear input buffer
            cin.ignore(numeric_limits<streamsize>::max(), '\n');    //         -||-
    }
    return true;
}

//-------------
// Print menu
// ------------
void UserInterface::printMenuOptions(){

    cout << '\n' << "****** Welcome to the menu ******\n\n";
    for (auto &e : MENU_OPTIONS){                               // MENU_OPTIONS is defined in
        cout << e << '\n';                                      // constants.h
    }

    cout << "\nEnter your choice: ";
}

//-------------
// Sorting
// ------------

void UserInterface::sortName(){
    cout << "Sorting by name!\n";
    personList.sortByName();
}

void UserInterface::sortPn(){
    cout << "Sorting by social security number!\n";
    personList.sortByPn();
}

void UserInterface::sortShoe(){
    cout << "Sorting by shoe size!\n";
    personList.sortByShoe();
}

// -----------------
// Add person
// -----------------
void UserInterface::add(){

    cout << "\n*** Add person *** \n";

    Person tmpPerson;

    // Get name
    string tmpFirst, tmpLast;
    cout << "First name: ";
    getline(cin, tmpFirst);
    cout << "Last name: ";
    getline(cin, tmpLast);
    tmpPerson.setName(tmpFirst, tmpLast);

    // Get address
    string tmpStreet, tmpPost, tmpCity;
    cout << "Address: ";
    getline(cin, tmpStreet);
    cout << "Zipcode: ";
    getline(cin, tmpPost);
    cout << "City: ";
    getline(cin, tmpCity);
    tmpPerson.setAddress(tmpStreet, tmpPost, tmpCity);

    // Get personal number and shoesize
    string tmpPn;
    int tmpShoe;
    cout << "Personal number: ";
    getline(cin, tmpPn);
    cout << "Shoe size: ";
    cin >> tmpShoe;
    tmpPerson.setPersNr(tmpPn);
    tmpPerson.setShoeNr(tmpShoe);
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');    // Flush buffer, remove newline left in stream

    personList.addPerson(tmpPerson);
}

// -----------------
// Printing functions
//------------------

void UserInterface::printPersonList(){

    cout << "\n*** Printing persons in list ***\n";

    for (auto &e: personList.getPersonList()){
        printPerson(e);
    }
}

void UserInterface::printPerson(const Person &p){

    // Gets data for subclasses
    Name tmpName = p.getName();
    Address tmpAddress = p.getAddress();

    // Print formatted member data
    cout << setw(15) << left << "Name: " << tmpName.getFirstName() << " " << tmpName.getLastName() << '\n';
    cout << setw(15) << left << "Address: " << tmpAddress.getStreetAddress() << '\n';
    cout << setw(15) << left << "Postcode:" << tmpAddress.getPostalNr() << " " << tmpAddress.getCity() << '\n';
    cout << setw(15) << left << "Person number: " << p.getPersNr() << '\n';
    cout << setw(15) << left << "Shoe size: " << p.getShoeNr() << "\n\n";
}

// -----------------
// Save list to file
//------------------
void UserInterface::save(){

    cout << "****** Save to file ******\n\n";
    cout << "Filename: ";
    string fileName;                            // User input
    getline(cin, fileName);
    personList.setFileName(fileName);
    personList.saveToFile();
}

// -------------------
// Read list from file
// -------------------
void UserInterface::read(){

    cout << "****** Read from file ******\n\n";
    cout << "Filename: ";
    string fileName;                            // User input
    getline(cin, fileName);
    personList.setFileName(fileName);
    personList.readFromFile();
}
