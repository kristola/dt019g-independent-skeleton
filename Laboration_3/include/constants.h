//
// Created by Isak Kristola on 2019-02-05.
//

#ifndef DT019G_CONSTANTS_H
#define DT019G_CONSTANTS_H

#include <vector>
#include <string>

const char DELIM = '|';                 // Delim char used for writing/reading operations

const std::vector<std::string> MENU_OPTIONS{
    "1: Add person to person list:",
    "2: Print person list",
    "3: Save list on file",
    "4: Read list from file",
    "5: Sort list by name",
    "6: Sort list by social security number",
    "7: Sort list by shoe size",
    "8: Exit"
};

#endif //DT019G_CONSTANTS_H
