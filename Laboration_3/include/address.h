// address.h
// Header file for Address class
//
// Isak Kristola
// Objektbaserad programmering i C++, VT2019
// Laboration 3, Operator Overloading

#ifndef _laboration_2_include_address_h_
#define _laboration_2_include_address_h_

#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include "constants.h"

class Address{
private:
    std::string streetAddress;
    std::string postalNr;
    std::string city;

public:
    // Constructors
    Address() = default;                                                                     // Default constructor
    Address(const std::string &street, const std::string &postal, const std::string &city);  // Initializing constructor

    // Overload operators
    bool operator<(const Address &address) const;
    bool operator==(const Address &address) const;


    // Setters
    void setStreetAddress(const std::string &inStr);
    void setPostalNr(const std::string &inPost);
    void setCity(const std::string &inCity);

    // Getters
    std::string getStreetAddress() const { return streetAddress; };
    std::string getPostalNr() const { return postalNr; };
    std::string getCity() const { return city; };

};

// Overload << and >> for file handling
std::ostream &operator<<(std::ostream &os, const Address &adr);
std::istream &operator>>(std::istream &is, Address &adr);

#endif
