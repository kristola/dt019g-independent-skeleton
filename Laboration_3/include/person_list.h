// person_list.h
// Header file for PersonList class
// Isak Kristola
// Objektbaserad programmering i C++, VT2019
// Laboration 3, Operator Overloading

#ifndef DT019G_PERSON_LIST_H
#define DT019G_PERSON_LIST_H

#include <algorithm>
#include "person.h"

class PersonList{
private:
    std::vector<Person> pList;
    std::string fileName = "default.txt";

public:
    // Constructors
    PersonList() = default;

    // Getters
    std::string getFileName() const { return fileName; }
    std::vector<Person> getPersonList() const { return pList; }
    size_t personListSize() { return pList.size() ;}
    bool getPerson(int idx, Person &p) const;

    // Setters
    void setFileName(const std::string &inFileName) { fileName = inFileName; }
    void addPerson(const Person &inPers) { pList.push_back(inPers); }

    // Sort
    void sortByName();
    void sortByPn();
    void sortByShoe();

    // File handling
    void saveToFile();
    void readFromFile();
};

// Related free functions (comparison helper functions)
bool nameCmp(const Person &p1, const Person &p2);
bool persNrCmp(const Person &p1, const Person &p2);
bool shoeCmp(const Person &p1, const Person &p2);

#endif //DT019G_PERSON_LIST_H
