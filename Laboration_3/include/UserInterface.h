// user_interface.h
// Header file for UserInterface class
//
// Isak Kristola, iskr1300
// Objektbaserad programmering i C++, VT2019
// Laboration 3, Operator Overloading

#ifndef DT019G_USER_INTERFACE_H
#define DT019G_USER_INTERFACE_H

#include "person_list.h"

class UserInterface{
private:
    // Data
    PersonList personList;

    // Functions
    bool runMenu();
    void add();
    void save();
    void read();
    void sortName();
    void sortPn();
    void sortShoe();
    void printPersonList();
    void printMenuOptions();
    void printPerson(const Person &p);

public:
    // Constructors
    UserInterface() = default;

    // Functions
    void run();

};

#endif //DT019G_USER_INTERFACE_H
