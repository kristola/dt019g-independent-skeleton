// person.h
// Header file for Person class
//
// Isak Kristola
// Objektbaserad programmering i C++, VT2019
// Laboration 3, Operator Overloading

#ifndef _laboration_2_include_person_h_
#define _laboration_2_include_person_h_

#include <iostream>
#include <string>
#include <vector>
#include <limits>
#include <iomanip>
#include "name.h"
#include "address.h"

class Person{
private:
    Name name;
    Address address;
    std::string persNr;
    int shoeNr = 0;
public:
    // Constructors
    Person() = default;                                         // Default constructor
    Person(const Name &aName, const Address &anAddress,         // Initializing constructor
            const std::string &aPersNr, const int &aShoeNr);

    // Overload operators
    bool operator<(const Person &person) const;
    bool operator==(const Person &person) const;

    // Setters
    void setPersNr(const std::string &inStr) { persNr=inStr; }
    void setShoeNr(const int &inShoeNr) { shoeNr=inShoeNr; }
    void setName(std::string &newFirst, std::string &newLast);
    void setAddress(std::string &newAddr, std::string &newPost, std::string &newCity);

    // Getters
    Name getName() const { return name; }
    Address getAddress() const { return address; }
    std::string getPersNr() const { return persNr; }
    int getShoeNr() const { return shoeNr; }

    // File handling
    void outNames(std::ostream &os) { os << name; }         // Read fr. name object to ostream using overloaded operator
    void outAddresses(std::ostream &os) { os << address; }
    void inNames(std::istream &is) { is >> name; }          // Read from inputstream into name object
    void inAddresses(std::istream &is) { is >> address; }
};

// Overload << and >> operators
std::ostream &operator<<(std::ostream &os, Person &person);
std::istream &operator>>(std::istream &is, Person &person);

#endif
