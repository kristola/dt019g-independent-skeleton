#Template repo for DT019G

This is an alternative version of [Erik's
skeleton](https://bitbucket.org/miundsv/dt019g_skeleton_repo). The difference is
that each assignment has its own `CMakeLists.txt` file which is included in the
top-level `CMakeLists.txt` file. This means that the project can be
build/compiled as a whole, or as individual entities. This makes it easy to
create an independent zip-archive for each of the assignments.

## Use

 + Simply fork this repo, and clone the fork with CLion or manually. If
   you clone it directly and still want to use your bitbucket repo as the remote
   repo you have to change the remote repo. That is easy enough, but a fork does
   what you want and i easier!
 + If you create a new source file, add its filename to the `CMakeLists.txt`
   file for the corresponding assignmnt, below the comment `# Add any new soruce
   files here...`. For instance add the new filename for lab1 to
   `Laboration_1/CMakeLists.txt`.
 + Header files in the `include/` directories will be detected automatically so
   just include them as usual, without the path. I.e to include a header file
   located in the `include` directory for one of the assignments:

```cpp
// Laboration_1/src/lab1.cpp

// To include Laboration_1/include/my_new_lib.h
#include "my_new_lib.h
```

If you use cmake to compile it should detect new source files and rebuild the
project.

## Switching between run configurations

In case this is not obvious it is possible to switch between executables in
CLion in the run configuration dropdown:

![Switching between run configurations](img/run-modes.png)

##Create zip-files automatically

I have added a custom build target that will create a zip-file, for each of the
assignments/project. To run this target simply select the corresponding target
from the configuration list in CLion, see the image above, e.g. `zip-lab1` and
hit the "hammer" to the left of the dropdown list. This will create a zip-file
with the necessary files and place the file in the `archives` directory in the
template root.

This can be done manually in the terminal as well:
```
build/$ cmake --build . --target zip-lab1
```

##Notes about adding new files to cmake

I have not used automatic detection of files in the build scripts, instead I
have chosen to add each new files explicitly to each target. This has the
benefit of letting `CMAKE` detect the changes to the `CMakeLists.txt` file and
automatically reload the build scipt before compiling, when a new file is added.

The drawback is that we have to manually insert the path to any new source files
that we add to one of the executables. But it is a simple matter of open up the
`CMakeLists.txt` file for the relevant assigment and att it below the line `#Add
any new source files here...` so it shouldn't be too hard. All header files
`*.h` in the `include` directory will be added automatically.

##Testing with catch2

There is also a separate branch where I have added testing functionality for
each assignment, using Catch2.

+ [Video presentation](https://www.youtube.com/watch?v=Ob5_XZrFQH0)
+ [Tutorial and reference](https://github.com/catchorg/Catch2).

Testing is outside the requirements for this course. But it is also very useful,
and something that interests me so I thought I would start learning how to do it
in `C++`. I figured I might as well upload this branch too, in case anyone else
shares this interest.

Both branches have been tested in the terminal using `git`, `cmake` (3.13) and
in CLion. I have not tested it in Visual Studio.

If you want to switch to the testing branch it is easily done in either the
terminal:

```
$ git checkout with-tests
```

or in CLion:

 + Click on `Git:master` in the lower right corner
 + Under `remote branches` select `with-tests` and `import as..`
 + Click `Ok` (default name will be fine)

Run the tests from by choosing the corresponding target in the dropdown list as
any other target. Or from within the build directory in the terminal:

```
build$ ctest --output-on-failure
```

I haven't added tests for lab2, since the assignments itself is creating
manual tests.

## Contributions and feedback

If you have any suggestions or found any problems, either commit the changes to
your fork, push the changes to bitbucket and use it to create a pull request or,
if you are unsure about how the issue can be resolved, create an issue
here or message me in any of the Discord or Ryver channels.
